import { exec } from 'pkg';
import dirname from './dirname.mjs';
import path from 'path';

const __dirname = dirname(import.meta);

const CONFIG = {
    source: path.resolve(__dirname, './build/enola.js'),
    output: path.resolve(__dirname, './dist/enola-[target].exe'),
    targets: {
        win64: 'node[v]-win-x64',
        // mac64: 'node[v]-macos',
        // linux64: 'node[v]-linux-x64',
    },
    nodejs: 18
};

const packing = async () => {
    for (let [target, platform] of Object.entries(CONFIG.targets)) {
        platform = platform.replace(/\[v\]/g, CONFIG.nodejs);
        const outfile = CONFIG.output.replace(/\[target\]/g, target);
        /* Build */
        console.log(`[BUILD] ${platform}\n  -> ${outfile}`);
        await exec([CONFIG.source, '-t', platform, '--output', outfile]);
    }
};
packing();
