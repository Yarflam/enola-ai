import commonjs from '@rollup/plugin-commonjs';
import noderesolve from '@rollup/plugin-node-resolve';
import { terser } from 'rollup-plugin-terser';
import json from '@rollup/plugin-json';
import dirname from './dirname.mjs';
import path from 'path';

const __dirname = dirname(import.meta);

export default {
    input: path.resolve(__dirname, './src/index.mjs'),
    output: {
        file: path.resolve(__dirname, './build/enola.js'),
        format: 'cjs',
    },
    plugins: [
        json(),
        commonjs({
            ignoreDynamicRequires: true,
        }),
        noderesolve(),
        terser({
            format: {
                comments: false,
            },
            compress: false,
        }),
    ],
};
