import Enola from './Enola.mjs';
import 'colors';

async function main() {
    const app = new Enola();
    await app.welcome();
    await app.configure();
    /* Talking */
    const NB_TRYING = 3;
    let trying = Number(NB_TRYING);
    let answer, lastPrompt, prompt = null;
    while(trying > 0 && !(
        prompt === '/quit' ||
        prompt === 'exit' ||
        prompt === 'quit'
    )) {
        trying--;
        /* Find an answer */
        if(prompt) {
            answer = await app.talk(prompt);
            if(!answer) continue;
            if(!(await app.reply(answer))) {
                app.backLastChatMsg();
                continue;
            }
        }
        /* Prompting */
        if(typeof prompt === 'string')
            lastPrompt = String(prompt);
        prompt = await app.prompt();
        if(!prompt) prompt = lastPrompt;
        trying = Number(NB_TRYING);
    }
}
main();