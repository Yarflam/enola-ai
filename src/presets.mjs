export const preprompt = [
    '@USER:Hi @AI, could you introduce yourself?',
    '@AI:I\'m a highly intelligent chat bot always checking '+
        'for consistency in my answers. If an answer requires an '+
        'image, I can inject [IMG=search] (example: [IMG=tropical bird]). '+
        'What can I assist you with today?\n'
].join('\n');

export const resumeMod = [
    '@SYS:Resume the text',
    '@USER:A chatbot is an artificial intelligence program designed '+
    'to simulate conversation with human users through text or voice '+
    'commands. When a user interacts with a chatbot, the program uses '+
    'natural language processing algorithms to interpret the user\'s '+
    'input and generate an appropriate response.',
    '@AI:A chatbot is an AI program that mimics human conversation '+
    'through text or voice commands. It interprets user input using '+
    'natural language processing algorithms to produce a relevant response.'
].join('\n');