import Consola from './Consola.mjs';
import HistoryTalk from './HistoryTalk.mjs';
import HuggingFace from './HuggingFace.mjs';
import InputTerm from './libs/InputTerm.js';
import Media from './Media.mjs';
import MemLocal from './MemLocal.mjs';
import dirname from '../dirname.mjs';
import uniqid from 'uniqid';
import path from 'path';
import fs from 'fs';
import { preprompt, resumeMod } from './presets.mjs';
import 'colors';

const __dirname = dirname(import.meta);

class Enola {
    constructor () {
        this._userName = 'You';
        this._agentName = 'Enola';
        this._systemName = 'System';
        this._userPrompt = `${this._userName}: `;
        this._agentPrompt = `${this._agentName}: `;
        this._systemPrompt = '# ';
        this._chatLinkPrompt = {
            [this._userName]: this._userPrompt,
            [this._agentName]: this._agentPrompt,
            [this._systemName]: this._systemPrompt
        };
        /* Data */
        this._memory = new MemLocal();
        this._chat = new HistoryTalk();
        this._chat.setPreprompt(this.__cleanPreprompt(preprompt));
        /* API tokens */
        this._huggingFaceToken = this._memory.getSecEntry(Enola.MEM_HGTOKEN);
        this._settings = {
            saveImg: this._memory.getEntry(Enola.MEM_SET_SAVEIMG)
        };
    }

    welcome () {
        Consola.log(`Load: ${this._memory.getDestPath()}\n`.gray);
        Consola.log('  ___  _  _  _  _     _   '.green);
        Consola.log(' | __|| \\| |/ \\| |   / \\  '.green);
        Consola.log(' | _| | \\\\ ( o ) |_ | o | '.green);
        Consola.log(' |___||_|\\_|\\_/|___||_n_| \n'.green);
        Consola.log(`${this._agentPrompt}Hello!`);
        return this;
    }

    async configure () {
        let isPerfect = false;
        /* HuggingFace Token */
        if(!this._huggingFaceToken) {
            Consola.log(`${this._agentPrompt}Please type your HuggingFace token.`);
            this._huggingFaceToken = await this.__promptConf(
                '> ',
                'password',
                value => {
                    if(!Enola.RGX_HGTOKEN.test(value)) {
                        Consola.log(`${this._agentPrompt}Wrong. It's look like hf_xxxxx.`);
                        return false;
                    }
                    return true;
                }
            );
            /* Save the token */
            this._memory.setSecEntry(Enola.MEM_HGTOKEN, this._huggingFaceToken);
            isPerfect = true;
        }
        /* Save images */
        if(typeof this._settings.saveImg !== 'boolean') {
            const fdir = path.resolve('./temp/');
            Consola.log(
                `${this._agentPrompt}Do you want save the generated images in\n` +
                `${Consola.space(this._agentPrompt.length)}${fdir}?`
            );
            const saveImg = (await this.__promptConf(
                'Yes|No> ',
                'text',
                value => {
                    if(!/^(y|yes|n|no)$/i.test(value)) return;
                    return true;
                }
            )).toLowerCase();
            /* Save the choice */
            this._settings.saveImg = saveImg === 'y' || saveImg === 'yes';
            this._memory.setEntry(Enola.MEM_SET_SAVEIMG, this._settings.saveImg);
            /* Create the folder */
            if(this._settings.saveImg) fs.mkdirSync(fdir, { recursive: true });
            isPerfect = true;
        }
        /* Sweet message */
        if(isPerfect) Consola.log(`${this._agentPrompt}Perfect! We can talk now.`);
        return this;
    }

    prompt () {
        return new Promise(resolve => {
            new InputTerm().prompt(this._userPrompt, resolve, 'text');
        });
    }

    async talk (prompt) {
        /* Replying */
        this._chat.add(this._userName, prompt);
        const inputs = this._chat.getTextHistory(this._chatLinkPrompt) + this._agentPrompt;
        if(Enola.DEBUG) console.log(`PROMPT\n${inputs.replace(/(^|\n)/g, '$1> ')}`.gray);
        /* Text answer */
        let answer = await HuggingFace.genText(inputs, {
            debug: Enola.DEBUG,
            token: this._huggingFaceToken,
            stop: Object.keys(this._chatLinkPrompt)
        });
        if(!answer) return null;
        if(Enola.DEBUG) console.log(`ANSWER\n${answer.replace(/(^|\n)/g, '$1> ')}`.gray);
        return answer;
    }

    async injectMedia (content) {
        let match, generate;
        while((match=Enola.RGX_IMG.exec(content),match!==null)) {
            if(!/^https?:/.test(match[1])) {
                /* Generate image */
                generate = await HuggingFace.genImage(
                    `${match[1].replace(/\.[\w]{3,4}$/g, '')}, realistic`,
                    {
                        debug: Enola.DEBUG,
                        token: this._huggingFaceToken,
                        negative: 'no realistic, blur, fuzzy, bad quality'
                    }
                );
                /* Save in temp folder */
                this.saveMediaImg(generate);
                /* Load Image -> ASCII */
                generate = await Media.showImage(generate) || '';
            }
            content = content.replace(
                match[0],
                generate ? `\n${generate}\n` : ''
            );
        }
        return content;
    }

    saveMediaImg (imgBuffer) {
        if(!imgBuffer || !this._settings.saveImg) return;
        const fpath = path.resolve(`./temp/${uniqid()}.png`);
        if(Enola.DEBUG) console.log(`${this._systemPrompt}Save image ${fpath}`.gray);
        fs.writeFileSync(fpath, Buffer.from(imgBuffer));
        return this;
    }

    async reply (message) {
        let display = await this.injectMedia(message);
        if(!display) return false;
        this._chat.add(this._agentName, message);
        Consola.log(`${this._agentPrompt}${display}`);
        return true;
    }

    backLastChatMsg () {
        this._chat.backLast();
        return this;
    }

    async __promptConf (prompt, type='text', check=null) {
        let out = null;
        while(!out) {
            out = await new Promise(resolve => {
                new InputTerm().prompt(prompt, resolve, type);
            });
            if(out === '/quit') process.exit();
            if(typeof check === 'function' && !check(out)) out = null;
        }
        return out;
    }

    __cleanPreprompt (chain) {
        return chain
            .replace(/@USER:/g, this._userPrompt)
            .replace(/@AI:/g, this._agentPrompt)
            .replace(/@SYS:/g, this._systemPrompt)
            .replace(/@USER/, this._userName)
            .replace(/@AI/, this._agentName)
            .replace(/@SYS/, this._systemName);
    }
}
Enola.DEBUG = process.argv.indexOf('--debug') >= 0;
Enola.RGX_HGTOKEN = /^hf_[\w]+$/;
Enola.MEM_HGTOKEN = 'hgToken';
Enola.MEM_SET_SAVEIMG = 'saveImg';
Enola.RGX_IMG = /\[IMG=([^\]]+)\]/g;

export default Enola;