class Consola {
    static log (...msg) {
        console.log(...msg);
    }

    static warn (...msg) {
        console.warn(...msg);
    }

    static error (...msg) {
        console.error(...msg);
    }

    static space (nb) {
        return new Array(nb).fill(' ').join('');
    }

    static getSize () {
        return {
            width: process.stdout.columns,
            height: process.stdout.rows
        };
    }
}

export default Consola;