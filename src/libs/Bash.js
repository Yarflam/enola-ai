const { exec } = require('child_process');

class Bash {
    static exec(cmd) {
        return new Promise((resolve) => this.execAsync(cmd, resolve));
    }

    static execAsync(cmd, callback) {
        exec(cmd, (err, stdout, stderr) => {
            let msg = { success: false, error: stderr, data: null };
            if (!err) {
                msg.success = true;
                msg.data = stdout;
            } else msg.code = err.code;
            if (typeof callback === 'function') callback(msg);
        });
    }

    static debug(msg, withSuccess = false) {
        try {
            if (!msg.success) console.error(msg.error);
            if (withSuccess && msg.success) console.log(msg.data);
        } catch (e) {
            console.error(e);
        }
    }
}

module.exports = Bash;
