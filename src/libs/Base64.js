class Base64 {
    static encode(c) {
        let o, r, a, l, i, t;
        for (
            a = Base64.ALPHA, o = '', r = 0, i = 0;
            ((l = c.charCodeAt(i)), l >= 0) ||
            ((l = 0), c[i - 1] && i % 3) ||
            ((a = '='), (r = l = 0), o.length % 4);
            !(i % 3) && c[i - 1] ? ((o += a[r]), (r = 0)) : 1
        ) {
            (t = ((i % 3) + 1) * 2),
            (o += a[r + (l >> t)]),
            (r = (l - ((l >> t) << t)) << (6 - t)),
            i++;
        }
        return o;
    }

    static decode(c) {
        let o, r, a, l, i, t;
        for (
            a = Base64.ALPHA, o = '', r = 0, i = 0;
            (l = a.indexOf(c[i])), l >= 0;
            i++
        ) {
            (t = (6 - (i % 4) * 2) % 6),
            i % 4
                ? ((o += String.fromCharCode(r + (l >> t))),
                (r = (l - ((l >> t) << t)) << (8 - t)))
                : (r = l << 2);
        }
        return o;
    }
}
Base64.ALPHA =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

module.exports = Base64;
