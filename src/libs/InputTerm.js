class InputTerm {
    constructor() {
        this._data = '';
        this._position = 0;
    }

    reset() {
        this._data = '';
        this._position = 0;
    }

    prompt(question, callback, type = 'text') {
        const isTTY = process.stdin.isTTY;
        if (isTTY) process.stdin.setRawMode(true);
        /* Initialize */
        const evtCapture = (buff) => {
            this.__capture(buff, { isTTY, evtCapture, callback, type });
        };
        process.stdin.on('data', evtCapture);
        /* Prompt */
        process.stdout.write(question);
        process.stdin.resume();
    }

    __capture(buff, { isTTY, evtCapture, callback, type }) {
        let chunk = buff.toString('utf8');
        let ckCodes = chunk.split('').map((x) => x.charCodeAt(0));

        /*
         *  Stopping points
         */
        /* TTY not supported */
        if (!isTTY) {
            this._data = chunk;
            return this.__stop(evtCapture, callback);
        }
        /* CTRL+C -> quit */
        if (ckCodes[0] === 3) {
            this._data = '/quit';
            return this.__stop(evtCapture, callback);
        }
        /* Finished */
        if (chunk === '\r') {
            if (callback.length >= 2 && !callback(null, this._data)) return;
            /* Continue */
            CursorIO.write('\n');
            return this.__stop(evtCapture, callback);
        }

        /*
         *  Treatments
         */
        /* Move */
        if (ckCodes[0] == 27 && ckCodes[1] == 91) {
            // LEFT
            if (ckCodes[2] === 68 && this._position >= 1) {
                CursorIO.left();
                this._position--;
            }
            // RIGHT
            else if (ckCodes[2] === 67 && this._position < this._data.length) {
                CursorIO.write(this._data[this._position]);
                this._position++;
            }
            return;
        }
        /* Space/Tab */
        if (ckCodes[0] == 9 || ckCodes[0] == 32) {
            if (!this._data.length) return;
            /* Tab */
            if (ckCodes[0] == 9) {
                chunk = new Array(4 - (this._position % 4)).fill(' ').join('');
            }
        }
        /* Back */
        if (ckCodes[0] === 8) { // 127?
            if (this._data.length) {
                CursorIO.left()
                    .space(this._data.length - this._position + 1)
                    .left(this._data.length - this._position + 1)
                    .write(this._data.slice(this._position))
                    .left(this._data.length - this._position);
                this._data = this._data.slice(0, this._position - 1) + this._data.slice(this._position);
                this._position--;
            }
            return;
        }

        /*
         *  Standard output
         */
        /* Direct output */
        if (type === 'password') {
            CursorIO.mWrite(chunk.length, '*'); // secret input
        } else CursorIO.write(chunk);
        /* Position */
        if (this._position < this._data.length) {
            CursorIO.write(this._data.slice(this._position)).left(this._data.length - this._position);
            this._data = this._data.slice(0, this._position) + chunk + this._data.slice(this._position);
        } else this._data += chunk;
        this._position += chunk.length;
    }

    __stop(evtCapture, callback) {
        process.stdin.removeListener('data', evtCapture);
        process.stdin.pause();
        callback(this._data);
        this.reset();
    }
}

class CursorIO {
    static write(data) {
        process.stdout.write(data);
        return CursorIO;
    }
    static mWrite(n, data) {
        new Array(n).fill(0).forEach(() => process.stdout.write(data));
        return CursorIO;
    }

    static left(n = 1) {
        return CursorIO.mWrite(n, '\x08');
    }

    static space(n = 1) {
        return CursorIO.mWrite(n, '\x20');
    }

    static topLine() {
        process.stdout.write('\x1b[A');
        return CursorIO;
    }

    static clearLine() {
        process.stdout.write('\x1b[K');
        return CursorIO;
    }
}

module.exports = InputTerm;
