class HistoryTalk {
    constructor() {
        this._history = [];
        this._preprompt = '';
        this._resume = '';
    }

    add (from, msg) {
        this._history.push({ from, msg });
        return this;
    }

    backLast () {
        this._history.pop();
        return this;
    }

    getTextHistory (linkPrompt, preprompt=HistoryTalk.PREPROMPT_AUTO) {
        let out = String(preprompt) || '';
        if(preprompt === HistoryTalk.PREPROMPT_AUTO)
            out = this._preprompt // + this._resume;
        for(let { from, msg } of this._history) {
            out += `${linkPrompt[from]||'Unknow: '}${msg}\n`;
        }
        return out;
    }

    getResume () {
        return this._resume;
    }

    setPreprompt (preprompt) {
        if(typeof preprompt === 'string')
            this._preprompt = preprompt;
        return this;
    }

    setResume (resume) {
        if(typeof resume !== 'string') return this;
        this._resume = resume;
        return this;
    }
}
HistoryTalk.PREPROMPT_AUTO = 'auto';

export default HistoryTalk;