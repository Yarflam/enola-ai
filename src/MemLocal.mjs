import Base64 from './libs/Base64.js';
import path from 'path';
import fs from 'fs';

class MemLocal {
    constructor() {
        this._state = this.getDefaultState();
        this._destPath = path.resolve('./enola-memory.json');
        this.load();
    }

    load() {
        if(fs.existsSync(this._destPath)) {
            try {
                this._state = JSON.parse(fs.readFileSync(this._destPath, 'utf-8'));
            } catch (e) {
                this._state = this.getDefaultState();
            }
        } else this.save();
        return this;
    }

    save() {
        fs.writeFileSync(this._destPath, JSON.stringify(this._state, false, 4));
        return this;
    }

    setEntry (name, value, save=true) {
        this._state[name] = value;
        if(save) this.save();
        return this;
    }
    setSecEntry (name, value, save=true) {
        if(typeof value === 'string')
            return this.setEntry(name, Base64.encode(value), save);
        return this;
    }

    getEntry (name, defaultValue=null) {
        return typeof this._state[name] !== 'undefined'
            ? this._state[name] : defaultValue;
    }
    getSecEntry (name, defaultValue=null) {
        const entry = this.getEntry(name, null);
        return typeof entry === 'string' ? Base64.decode(entry) : defaultValue;
    }

    getDefaultState () {
        return {
            version: MemLocal.VERSION
        };
    }

    getDestPath () {
        return this._destPath;
    }
}
MemLocal.VERSION = '1.0.0';

export default MemLocal