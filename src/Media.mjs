import terminalImage from 'terminal-image';
import fetch from 'node-fetch';
import fs from 'fs';

class MediaDriver {
    static async showImage (source) {
        /* From URL */
        if(typeof source === 'string' && /^https?:/.test(source)) {
            const url = String(source);
            source = resolve => {
                fetch(url)
                    .then(res => res.arrayBuffer())
                    .then(res => resolve(res))
                    .catch((e) => {
                        console.error(e);
                        resolve(null);
                    });
            };
        }
        /* From Callback */
        if(typeof source === 'function') {
            source = await new Promise(source);
        }
        /* From Buffer */
        if(source instanceof ArrayBuffer) {
            try {
                return await terminalImage.buffer(Buffer.from(source, { width: '90%' }));
            } catch(e) {
                return null;
            }
        }
        /* From File */
        if(typeof source === 'string' && fs.existsSync(source)) {
            try {
                return await terminalImage.file(source, { width: '90%' });
            } catch(e) {
                return null;
            }
        }
        return null;
    }
}

export default MediaDriver