import Consola from './Consola.mjs';
import fetch from 'node-fetch';

class HuggingFace {
    static request (model, payload={}, options={}) {
        return fetch(HuggingFace.API_URL.replace('%s', model), {
            method: 'POST',
            headers: {
                accept: '*/*',
                'content-type': 'application/json',
                'x-use-cache': false,
                Authorization: `Bearer ${options.token}`
            },
            body: JSON.stringify(payload)
        });
    }

    static async genText (inputs, options={}) {
        try {
            let out;
            while(!out) {
                let req = await this.request(HuggingFace.API_MODEL_TEXT, {
                    inputs,
                    parameters: {
                        seed: Math.floor(Math.random()*255),
                        // early_stopping: true,
                        length_penalty: 1,
                        max_new_tokens: 255,
                        do_sample: true,
                        temperature: 0.7,
                        top_p: 0.9,
                        stop: options.stop
                    }
                }, options);
                /* Error */
                let json = await req.json();
                if(typeof json === 'object' && typeof json.error === 'string') {
                    if(json.error.indexOf('loading') >= 0) {
                        await this.__waiting(3);
                        continue;
                    }
                    out = json.error;
                } else {
                    out = json[0].generated_text.replace(inputs, '');
                }
            }
            /* Treatment */
            for(let search of options.stop) {
                if(out.indexOf(search, out.length-search.length) >= 0) {
                    out = out.substr(0, out.length-search.length);
                }
            }
            return out.replace(/(^[\s]|[\s]$)/g, '');
        } catch(e) {
            if(options.debug) Consola.error(e);
            return null;
        }
    }

    static async genImage (inputs, options={}) {
        try {
            let req = await this.request(HuggingFace.API_MODEL_IMAGE, {
                inputs,
                parameters:{
                    width: 512,
                    height: 512,
                    num_inference_steps: 30,
                    guidance_scale: 15,
                    ...(
                        options.negative &&
                        typeof options.negative === 'string'
                            ? { negative_prompt: options.negative }
                            : {}
                    )
                }
            }, options);
            return await req.arrayBuffer();
        } catch(e) {
            if(options.debug) Consola.error(e);
            return null;
        }
    }

    static __waiting (sec) {
        return new Promise(resolve => setTimeout(resolve, sec * 1000));
    }
}
HuggingFace.API_URL = 'https://api-inference.huggingface.co/models/%s';
HuggingFace.API_MODEL_TTS = 'kan-bayashi/ljspeech_vits';
HuggingFace.API_MODEL_TEXT = 'bigscience/bloom';
HuggingFace.API_MODEL_IMAGE = 'runwayml/stable-diffusion-v1-5';
HuggingFace.API_MODEL_EMBEDDING = 'sentence-transformers/all-MiniLM-L6-v2';

export default HuggingFace;