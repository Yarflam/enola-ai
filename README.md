# Enola AI

![license](https://img.shields.io/badge/license-CC_BY--NC--SA-green.svg)

## Install

Support Node.js versions `14` -> `18` (maybe more).

```bash
git clone https://gitlab.com/Yarflam/enola-ai.git
cd enola-ai
npm i
```

## Usage

**Run the source code**

```bash
npm start
```

The first time you need to configure it:

```text
Load: C:\...\enola-ai\enola-memory.json

  ___  _  _  _  _     _
 | __|| \| |/ \| |   / \
 | _| | \\ ( o ) |_ | o |
 |___||_|\_|\_/|___||_n_|

Enola: Hello!
Enola: Please type your HuggingFace token.
> *************************************
Enola: Do you want save the generated images in
       C:\...\enola-ai\temp?
Yes|No> Yes
Enola: Perfect! We can talk now.
You:
```

**Build the executable**

```bash
npm run build
```

Note: you can uncomment the **MacOS** and **Linux** versions in the _enola-packing.mjs_ file.

## Capture

![Enola](capture.png)

## Authors

-   Yarflam - _initial work_

## License

The project is licensed under Creative Commons (BY-NC-SA).
