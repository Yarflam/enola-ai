import { fileURLToPath } from 'url';

/*
 *  usage:
 *      import dirname from 'path/to/dirname';
 *      const __dirname = dirname(import.meta);
 */
function dirname(meta) {
    return fileURLToPath(new URL('.', meta.url));
}

export default dirname;
